# Practica ArquitecturaBDKeepcoding

## Idea general

Sistema de recomendación de precios y alojamientos airbnb en función de las características, datos entorno(apis publicas)
cómo agenda, eventos, ocio, actividades deportivas, parkings públicos (precio y disponibilidad), 
restaurantes tipo por host, tiendas, bares etc.

## Nombre del Producto

Sistema pricing dinamico y recomendación alojamientos airbnb en Madrid

### Estrategia del DAaaS

Realizar una recomendación de precio en cada noche del año para cada host en base a los datos de airbnb sobre el host y datos
de caracter público que se obtendrán de apis de Madrid, foursquare y openstreetmap.

Los datos públicos que se van a ingestar son: actividades, eventos, parkings públicos (precio, y ocupación), transporte, 
restaurantes, bares, tiendas...Para ello se montará un desarrollo scrapy que integre todas estas fuentes en una.

La ingesta de datos se realizará en procesos batch todos los días y se ira almacenando un histórico para que el motor de pricing
vaya actualizandose con los nuevos datos. 

Por otro lado, el sistema realizará una recomendación de los alojamientos más compatibles con los clientes en función a unos 
datos de entrada que completará en los parametros de la búsqueda a través de una interfaz web/app. El resultado final será
un precio dinámico para esas fechas y unos alojamientos recomendados para el cliente en función de su pérfil. El valor del sistema
residirá en clusterizar(segmentar) los alojamientos y relacionarlos con los clientes en función al perfil, las fechas y los intereses.


### Arquitectura

La arquitectura de este sistema se va a desarrollar sobre **"Google Cloud Platform".**

Los componentes de la arquitectura son los siguientes: Scrapy, Google Cloud Storage, HIVE, Dataproc, CloudScheduler. Además se utilizarán
diferentes librerías de machine learning para poder desarrollar el motor de recomendación y técnicas de series temporales con deep learning para
la predicción de precios.

1.-Se va a realizar mediante el uso de APIRest  la ingesta de los diferentes origentes de datos que se van a incluir para
el enriquecimiento de la información. Tal y cómo se ha comentado en el punto referido a la estrategia, se van a usar diferentes origentes
cómo OpenStreepMap, Datos públicos de Madrid y Foursquare. La frecuencia de ingesta de estos datos se realizará mediante un proceso
batch una vez al día.

2.-Mediante el uso de scrapy, se va a realizar la ingesta diaría también, de los datos referentes a Airbnb. El objetivo de esta acción,
es mantener actualizados diariamente toda la información relativa a los host.

3.-Los ficheros generados del punto 1 y 2 se almacenarán en el bucket(segmento) del cloud storage que se creará para mantener los datos
en staging. Estos datos son los que se utilizarán para crear el modelo de datos que alimentará a los diferentes  modelos. Se crearán
ficheros diarios que podrán historificar toda la información.

4.-Se va a diseñar un modelo de datos en Hive que va a permitir poder trabajar con la informacion y poder realizar el tratamiento entre ficheros
necesario. Se hará uso de la librería **pyhive** para realizar las diferentes joins y transformaciones generando jobs map reduce.
Estos resultados se alojaran de nuevo en el bucket.
El proceso de alimentación y actualización de los modelos también se realizará diariamente.

5.-El usuario de la interfaz web/app introducirá una serie de datos al sistema. El sistema devolverá un listado de alojamientos personalizados
con precios tarificados en función de todas las variables de los alojamientos más el usuario.


### Operating Model

La arquitectura en la que se va a basar nuestro sistema va a ser Google Cloud. En el componente Dataproc se va a configurar 1 cluster,
con unas características determinadas que serán escaladas en función de las necesidades de datos que se requieran y estará operativo
las 24 horas del día. En un primer momento se dispondrá de 1 maquina maestra y 2 maquinas esclavas.

En la arquitectura diseñada se van a diferenciar 2 tipos de ejecuciones: procesos batch de ingesta y actualización del dato y 
1 proceso online mediante trigger en el que se devolverá una serie de resultados. Ambos procesos utilizan cloud functions para activar
el desarrollo de cada tarea.

A continuación, se va a explicar brevemente el modelo operativo de nuestro DaaS:

El sistema necesitará que cada día se realice una ingesta y actualización de datos provenientes de diferentes APIs. Para ello
se va a utilizar la herramienta CloudScheduler que permitirá que se lancen una serie de procesos en batch y otro online cuando 
el cliente realiza la petición de datos:

    
    1.-Ingesta de datos mediante scrapy y APIRest se almacenan en el bucket del cloud storage en la carpeta /input.
		Estos datos se realizarán en el script Scrapping_datos.py. 
			Los ficheros que se van a generar son:
				-Datos_Api_Publica.csv y Datos_Api_Airbnb.csv.
		La CloudFunction de activación de la tarea será:Scrapping
		El script estará almacenado en la carpeta /scripts del bucket.
	2.-Una vez almacenados en input, se van a ejecutar varios scripts que se almacenan en la carpeta /scripts.
	    
		2.1.-El CloudScheduler lanza la CloudFunction **"Scrapper** que ejecutará el script carga_modelo_datos.py. 
		Se realizará la carga de los ficheros csv en las tablas en hive, procesará los datos y los dejará preparados para que se pueda
		crear el modelo de recomendación.
		2.2-El CloudScheduler lanza la CloudFunction **"CargarModeloDatos"**.El script modelo_recomendacion.py, se alimentará de los datos 
		ya generados en el anterior proceso y desarrollará un modelo de recomendación que será utilizado a posteriori. El modelo generado, se almacenará
		en la carpeta /modelo en el formato hd5. El fichero se llamará modelo_recomendación.hd5.
		2.3-El CloudScheduler lanza la CloudFunction **"ML"**.
		El script pricing_alojamientos.py, se alimentará también de los datos ya generados en el anterior proceso
		y desarrollará un modelo de pricing para cada alojamiento en diferentes escenarios. El modelo generado, se almacenarán
		en la carpeta /modelo en el formato hd5. El fichero se llamará modelo_pricing.hd5
		
	3.-Una vez ya se ha realizado el modelo y se ha actualizado en el día, el sistema quedará a la espera de obtener
	resultados de peticiones de un cliente externo. 
		3.1-A través de una interfaz web/app el cliente introducirá una serie de datos.
		3.2-Se activará un trigger que ejecutará el cloud function "**Datos_Cli**" y almacenara en la carpeta input del bucket en 1 ficheros
		3.2-Posteriormente se ejecutará el proceso en python ejec_results_model.py que devolverá los resultados del modelo
		de recomendación para este cliente. Los resultados serán una serie de alojamientos recomendados con los precios 
		recomendados para el cliente y la fecha. Los resultados se almacenarán en output/ en el fichero resultados_cli.csv.
		3.3-Los resultados se visualizarán a través de la interfaz web/app.


### Diagrama

https://docs.google.com/drawings/d/1kzaoKAza7OdTBc2wxj6AilfhD7tf61FRQlnIQuikdIw/edit?usp=sharing